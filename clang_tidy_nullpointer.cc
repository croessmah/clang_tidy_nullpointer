//clang-tidy --checks=*,-modernize*,-llvmlibc-*,-cppcoreguidelines-*,-hicpp-* clang_tidy_nullpointer.cc -- clang_tidy_nullpointer.cc
#include <cstdio>

int foo(int **p1)
{
	printf("ok: %p\n", p1);//ice cream
	return **p1;
}

int main() try
{
	int* p = nullptr;

	printf("result: %d", foo(&p));
	fflush(stdin);
}
catch(...){}//make clang-tidy happy